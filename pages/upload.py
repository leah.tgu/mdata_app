import io
import os
import tempfile

import mdata.core
import streamlit as st
from mdata.io import read_machine_data_zip
from streamlit.runtime.uploaded_file_manager import UploadedFile

from logic.switch_page import switch_page

st.set_page_config(layout="wide")
st.title('Machine Data Upload')

maed_file = st.file_uploader("Upload a .maed/.zip file", type=['.zip', '.maed'])

if st.button('clear upload'):
    st.cache_data.clear()
    if 'uploaded_md' in st.session_state:
        del st.session_state['uploaded_md']

def read_zip(zf: UploadedFile) -> mdata.core.MDV2:
    return read_machine_data_zip(zf.getvalue(), validity_checking=False)


@st.cache_data
def attempt_import():
    if maed_file is not None:
        return read_zip(maed_file)


if maed_file is not None:
    md = attempt_import()
else: md = None

# selected_widget = st.sidebar.selectbox('Widget', available_widgets, format_func=lambda w: w.value)

if md is not None:
    st.session_state['uploaded_md'] = md
    switch_page('visualization')

#  generate_page(md, selected_widget)
