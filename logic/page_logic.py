from collections.abc import Mapping
from enum import Enum

import pandas as pd
import streamlit as st
from mdata.core import ObservationKinds, MachineDataV2
from mdata.core.factory import as_v2
from mdata.core.header import ObservationSpec
from mdata.core.protocols import TSSpec
from mdata.visualization import plotting, matplot
import plotly.express as px

class Widgets(Enum):
    Specifications = 'Specifications'
    RawData = 'Raw Data'
    Overview = 'Overview'
    Inspector = 'Inspector'
    #Dummy = 'Dummy'
    SegmentHists = 'Segment Histograms'

available_widgets = [w for w in Widgets]


def generate_page(md: MachineDataV2, widget):
    md = as_v2(md)

    c = st.columns(3)
    c[0].metric('#Observations', md.observation_count)
    c[1].metric('#Event Types', len(md.event_specs))
    c[2].metric('#Measurement Types', len(md.measurement_specs))

    c = st.columns(2)
    cont = c[0].container()
    cont.text(md.observation_index['time'].min())
    cont.caption('First Observation')
    cont = c[1].container()
    cont.text(md.observation_index['time'].max())
    cont.caption('Last Observation')

    if widget == Widgets.Specifications:
        st.markdown('## Events')
        st.table(make_spec_df(md.event_specs))
        st.markdown('## Measurements')
        st.table(make_spec_df(md.measurement_specs))
        #fig = plotting.create_measurement_frequency_plot(md)
        #st.plotly_chart(fig)

    elif widget == Widgets.RawData:

        c1, c2 = st.columns(2)
        with c1:
            st.markdown('## Index')
            st.write(md.observation_index)
        with c2:

            selection = st.selectbox('Observation Spec',
                                     [tsc.series_spec.kind + ':' + tsc.series_spec.type_name for tsc in
                                      md.series_containers])
            if selection is not None:
                label = selection[2:]
                if selection[0] == ObservationKinds.E:
                    st.write(md.get_events(label).df)
                if selection[0] == ObservationKinds.M:
                    st.write(md.get_measurements(label).df)

    elif widget == Widgets.Overview:
        f = plotting.create_overview_plot(md, downsample_to=5_000)
        st.plotly_chart(f, use_container_width=True)

    elif widget == Widgets.Inspector:
        c1, c2, c3 = st.columns(3)
        event_specs_list = list(md.event_series.keys())
        measurement_spec_list = list(md.measurement_series.keys())
        selected_measurement_spec = c1.selectbox('Measurement Spec', measurement_spec_list)
        fs, object_list = [], []
        if selected_measurement_spec is not None:
            fs = list(md.measurement_specs[selected_measurement_spec].features)
            object_list = sorted(md.measurement_series[selected_measurement_spec].objects)
        selected_feature = c1.multiselect('Feature Selection', fs)

        event_selection = c2.multiselect('Event Spec Selection', event_specs_list)
        plot_separately = c2.checkbox('plot separately', False)

        object_selection = c3.selectbox('Object Selection', object_list)
        # TODO remove
        md.measurement_series[selected_measurement_spec].df = md.measurement_series[selected_measurement_spec].df.astype({f: 'float64' for f in selected_feature})
        f = plotting.create_timeseries_plot(md, measurement_spec=selected_measurement_spec, obj=object_selection,
                                            events=event_selection, features=selected_feature,
                                            split_into_subplots=plot_separately)

        st.plotly_chart(f, use_container_width=True)

    # elif widget == Widgets.Dummy:
    #     measurement_spec_list = list(md.measurement_specs)
    #     selected_measurement_spec = st.selectbox('Measurement Spec', measurement_spec_list)
    #     if selected_measurement_spec is not None:
    #         f = matplot.create_basic_stacked_subplots(md, measurement_spec=selected_measurement_spec)
    #         st.write(f)

    elif widget == Widgets.SegmentHists:
        c1, c2 = st.columns(2)

        event_specs_list = list(md.event_series.keys())
        start_event = c1.selectbox('Event that marks segment start', event_specs_list)
        end_event = c2.selectbox('Event that marks segment end', event_specs_list)

        c1, c2 = st.columns(2)
        measurement_spec_list = list(md.measurement_series.keys())
        selected_measurement_spec = c1.selectbox('Measurement Spec', measurement_spec_list)
        fs = []
        if selected_measurement_spec is not None:
            fs = list(md.measurement_specs[selected_measurement_spec].features)
        selected_feature = c2.selectbox('Feature', fs)

        if start_event is not None and end_event is not None and selected_measurement_spec and selected_feature is not None:
            starts = md.get_events(start_event).df.time
            ends = md.get_events(end_event).df.time
            if len(starts) != len(ends):
                return
            intervals = pd.IntervalIndex.from_arrays(starts, ends, closed='left')
            sensor_1_copy = md.get_measurements(selected_measurement_spec).feature_column_view(include_object_col=True).copy()
            from mdata.core.extensions.metadata import feature_typing
            # TODO fix and remove data type stuff
            sensor_1_copy = sensor_1_copy.convert_dtypes()

            sensor_1_copy['segment'] = pd.cut(sensor_1_copy.time, bins=intervals).map(
                {iv: f'cycle {i}' for i, iv in enumerate(intervals)})
            sensor_1_copy_nna = sensor_1_copy[~sensor_1_copy.segment.isna()]
            # sensor_1_copy = sensor_1_copy.set_index('segment')
            f = selected_feature  # sensor_1_md.timeseries_spec.features[0]

            print(intervals)
            print(f)
            print(sensor_1_copy_nna[f].dtype)
            sensor_1_copy_nna = sensor_1_copy_nna.astype({f: 'float64'})
            print(sensor_1_copy_nna[f].dtype)

            mi, ma = sensor_1_copy_nna[f].min(), sensor_1_copy_nna[f].max()

            fig = px.histogram(sensor_1_copy_nna, x=f, range_x=[mi, ma], color='object', animation_frame='segment',
                         histnorm='probability', title='Histogram')
            st.plotly_chart(fig, use_container_width=True)

    else:
        st.write('Invalid Widget')


def make_spec_df(spec_types: Mapping[str, TSSpec]):
    df = pd.DataFrame.from_dict({label: list(spec.features) for label, spec in spec_types.items()},
                                orient='index').replace(pd.NA, '')
    df.columns = [f'f_{i}' for i in range(1, len(df.columns) + 1)]
    return df
